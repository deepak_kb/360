/*
Copyright IBM Corp. 2016 All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

		 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main


import (
	"fmt"
	"strconv"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	pb "github.com/hyperledger/fabric/protos/peer"
)

var logger = shim.NewLogger("loyalty")

// SimpleChaincode example simple Chaincode implementation
type SimpleChaincode struct {
}

func (t *SimpleChaincode) Init(stub shim.ChaincodeStubInterface) pb.Response  {
	logger.Info("########### loyalty Init ###########")

	_, args := stub.GetFunctionAndParameters()
	var hotel, resort,alice,bob,joe string    // Entities
	var hotel_balance, resort_balance,alice_balance,bob_balance,joe_balance int // Asset holdings
	var err error

	// Initialize the chaincode
	hotel = args[0]
	hotel_balance, err = strconv.Atoi(args[1])
	if err != nil {
		return shim.Error("Expecting integer value for asset holding")
	}
	resort = args[2]
	resort_balance, err = strconv.Atoi(args[3])
	if err != nil {
		return shim.Error("Expecting integer value for asset holding")
	}

	alice = args[4]
	alice_balance, err = strconv.Atoi(args[5])
	if err != nil {
		return shim.Error("Expecting integer value for asset holding")
	}

	bob = args[6]
	bob_balance, err = strconv.Atoi(args[7])
	if err != nil {
		return shim.Error("Expecting integer value for asset holding")
	}

	joe = args[8]
	joe_balance, err = strconv.Atoi(args[9])
	if err != nil {
		return shim.Error("Expecting integer value for asset holding")
	}
	logger.Info("hotel_balance = %d, resort_balance = %d\n", hotel_balance, resort_balance)

	// Write the state to the ledger
	err = stub.PutState(hotel, []byte(strconv.Itoa(hotel_balance)))
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(resort, []byte(strconv.Itoa(resort_balance)))
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(alice, []byte(strconv.Itoa(alice_balance)))
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(bob, []byte(strconv.Itoa(bob_balance)))
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(joe, []byte(strconv.Itoa(joe_balance)))
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(nil)

}

// Transaction makes payment of X units from hotel to resort
func (t *SimpleChaincode) Invoke(stub shim.ChaincodeStubInterface) pb.Response {
	logger.Info("########### loyalty Invoke ###########")

	function, args := stub.GetFunctionAndParameters()
	
	if function == "delete" {
		// Deletes an entity from its state
		return t.delete(stub, args)
	}

	if function == "query" {
		// queries an entity state
		return t.query(stub, args)
	}
	if function == "exchangeCeasor" {
		// Deletes an entity from its state
		return t.exchangeCeasor(stub, args)
	}
	if function == "transact" {
		// Deletes an entity from its state
		return t.transact(stub, args)
	}

	logger.Errorf("Unknown action, check the first argument, must be one of 'delete', 'query', or 'exchangeCeasor'. But got: %v", args[0])
	return shim.Error(fmt.Sprintf("Unknown action, check the first argument, must be one of 'delete', 'query', or 'exchangeCeasor'. But got: %v", args[0]))
}

func (t *SimpleChaincode) exchangeCeasor(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	// must be an invoke
	var hotel, resort string    // Entities
	var hotel_balance, resort_balance int // Asset holdings
	var X int          // Transaction value
	var err error

	if len(args) != 3 {
		return shim.Error("Incorrect number of arguments. Expecting 4, function followed by 2 names and 1 value")
	}

	hotel = args[0]
	resort = args[1]

	// Get the state from the ledger
	// TODO: will be nice to have a GetAllState call to ledger
	Hotelbytes, err := stub.GetState(hotel)
	if err != nil {
		return shim.Error("Failed to get state")
	}
	if Hotelbytes == nil {
		return shim.Error("Entity not found")
	}
	hotel_balance, _ = strconv.Atoi(string(Hotelbytes))

	Resortbytes, err := stub.GetState(resort)
	if err != nil {
		return shim.Error("Failed to get state")
	}
	if Resortbytes == nil {
		return shim.Error("Entity not found")
	}
	resort_balance, _ = strconv.Atoi(string(Resortbytes))

	// Perform the execution
	X, err = strconv.Atoi(args[2])
	if err != nil {
		return shim.Error("Invalid transaction amount, expecting a integer value")
	}
	hotel_balance = hotel_balance - X
	resort_balance = resort_balance + X
	logger.Infof("hotel_balance = %d, resort_balance = %d\n", hotel_balance, resort_balance)

	// Write the state back to the ledger
	err = stub.PutState(hotel, []byte(strconv.Itoa(hotel_balance)))
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(resort, []byte(strconv.Itoa(resort_balance)))
	if err != nil {
		return shim.Error(err.Error())
	}

        return shim.Success(nil);
}

func (t *SimpleChaincode) transact(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	// if len(args) != 2 {
	// 	return shim.Error( len(args))
	// }

	Debitbytes, err := stub.GetState(args[0])
	if err != nil {
		return shim.Error("Failed to get state")
	}
	if Debitbytes == nil {
		return shim.Error("Entity not found")
	}
	debit_balance, _ := strconv.Atoi(string(Debitbytes))

	Creditbytes, err := stub.GetState(args[1])
	if err != nil {
		return shim.Error("Failed to get state")
	}
	if Creditbytes == nil {
		return shim.Error("Entity not found")
	}
	credit_balance, _ := strconv.Atoi(string(Creditbytes))

	// Perform the execution
	X, err := strconv.Atoi(args[2])
	if err != nil {
		return shim.Error("Invalid transaction amount, expecting a integer value")
	}
	debit_balance = debit_balance - X
	credit_balance = credit_balance + X
	logger.Infof("debit_balance = %d, credit_balance = %d\n", debit_balance, credit_balance)

	// Write the state back to the ledger
	err = stub.PutState(args[0], []byte(strconv.Itoa(debit_balance)))
	if err != nil {
		return shim.Error(err.Error())
	}

	err = stub.PutState(args[1], []byte(strconv.Itoa(credit_balance)))
	if err != nil {
		return shim.Error(err.Error())
	}

    return shim.Success(nil);


}

// Deletes an entity from state
func (t *SimpleChaincode) delete(stub shim.ChaincodeStubInterface, args []string) pb.Response {
	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	hotel := args[0]

	// Delete the key from the state in ledger
	err := stub.DelState(hotel)
	if err != nil {
		return shim.Error("Failed to delete state")
	}

	return shim.Success(nil)
}

// Query callback representing the query of a chaincode
func (t *SimpleChaincode) query(stub shim.ChaincodeStubInterface, args []string) pb.Response {

	var hotel string // Entities
	var err error

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting name of the person to query")
	}

	hotel = args[0]

	// Get the state from the ledger
	Hotelbytes, err := stub.GetState(hotel)
	if err != nil {
		jsonResp := "{\"Error\":\"Failed to get state for " + hotel + "\"}"
		return shim.Error(jsonResp)
	}

	if Hotelbytes == nil {
		jsonResp := "{\"Error\":\"Nil amount for " + hotel + "\"}"
		return shim.Error(jsonResp)
	}

	jsonResp := "{\"Name\":\"" + hotel + "\",\"Amount\":\"" + string(Hotelbytes) + "\"}"
	logger.Infof("Query Response:%s\n", jsonResp)
	return shim.Success(Hotelbytes)
}

func main() {
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		logger.Errorf("Error starting Simple chaincode: %s", err)
	}
}
